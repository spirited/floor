package main

import "os"

// Help is displayed when after an erroneous command-line option is parsed, or
// the user specifies the help option flag. This function should override the
// usage function in the flag package for the entire application.
func help() {
    println(`
NAME
    Floor - Conquer Online Floor Editor
    Copyright 2020 Gareth Jensen, "Spirited"
    Build 2020.01.24

ABSTRACT
    floor [-b block] [-s surface] [-e elevation] dmapfile x y

DESCRIPTION
    Floor is a program for modifying Conquer Online data maps (DMaps) with 
    custom cell definitions. Each cell of a map contains a coordinate's block
    toggle, surface feedback type, and elevation. The new or current values of
    the cell will be outputed after each command.

ARGUMENTS
    dmapfile   File path to the data map to be read or modified
    x          The X coordinate of the cell to be read or modified
    y          The Y coordinate of the cell to be read or modified

OPTIONS
    -b / -block      Replaces the block bit for the cell, 1 is blocked
    -s / -surface    Replaces the surface feedback type for the cell
    -e / -elevation  Replaces the signed elevation for the cell 
    -v / -verbose    Enables verbose logging (false)
    -? / -help       Shows help documentation and exits the program (false)

EXAMPLES
    floor desert.dmap 400 400
    floor -block 1 desert.dmap 400 400
    floor -surface 0 desert.dmap 400 400
    floor -elevation 250 desert.dmap 400 400`)
    os.Exit(0)
}
